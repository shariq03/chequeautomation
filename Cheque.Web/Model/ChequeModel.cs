﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cheque.Web.Model
{
    public class ChequeModel
    {
        [Required]
        public string Payee { get; set; }

        [Required]
        [Range(1, 9999999999999, ErrorMessage = "Please enter valid Amount")]
        public double Amount { get; set; }

        public string AmountInWords { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }
    }
}
