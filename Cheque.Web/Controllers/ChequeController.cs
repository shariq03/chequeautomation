﻿using Cheque.Core.Contracts;
using Cheque.Web.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Cheque.Web.Controllers
{
    public class ChequeController : Controller
    {
        private IConverter _converter;
        public ChequeController(IConverter converter)
        {
            _converter = converter;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayResult(ChequeModel cheque)
        {
            if (ModelState.IsValid)
            {
                cheque.AmountInWords = _converter.Convert(cheque.Amount);
                cheque.Date = cheque.Date.Date;
                return View("Index", cheque);
            }
            return View("Index");
        }
    }
}