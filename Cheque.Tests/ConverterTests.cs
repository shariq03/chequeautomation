﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cheque.Core.Contracts;
using Cheque.Core.Implementation;

namespace Cheque.Tests
{
    [TestClass]
    public class ConverterTests
    {
        private IConverter _converter;
        [TestInitialize]
        public void Initialize()
        {
            _converter = new Converter();
        }

        [TestMethod]
        public void NumberToTextConversion_Returns_Text_WithFraction()
        {
            string result = _converter.Convert(153.87);
            Assert.AreEqual("One hundred fifty three dollars and eighty seven cents", result);
        }

        [TestMethod]
        public void NumberToTextConversion_Returns_Text()
        {
            string result = _converter.Convert(1530);
            Assert.AreEqual("One thousand five hundred thirty dollars", result);
        }

        [TestMethod]
        public void NumberToTextConversion_Returns_TextWithAllZeros()
        {
            string result = _converter.Convert(3005000150.65);
            Assert.AreEqual("Three billion, five million one hundred fifty dollars and sixty five cents", result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberToTextConversion_Returns_Text_Zero()
        {
            _converter.Convert(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberToTextConversion_Returns_Exceptions_MinusNumbers()
        {
            _converter.Convert(-153);
        }

        [TestMethod]
        public void NumberToTextConversion_Returns_Text_Million()
        {
            string result = _converter.Convert(1000000);
            Assert.AreEqual("One million dollars", result);
        }

        [TestMethod]
        public void NumberToTextConversion_Returns_Text_Billion()
        {
            string result = _converter.Convert(1000000000);
            Assert.AreEqual("One billion dollars", result);
        }

        [TestMethod]
        public void NumberToTextConversion_Returns_Text_Hundred_Billion()
        {
            string result = _converter.Convert(100000000000);
            Assert.AreEqual("One hundred billion dollars", result);
        }

    }
}
