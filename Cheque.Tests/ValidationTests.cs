﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cheque.Core.Contracts;
using Cheque.Core.Implementation;
using Cheque.Web.Controllers;
using Cheque.Web.Model;
using Moq;
using System.Web.Mvc;

namespace Cheque.Tests
{
    [TestClass]
    public class ValidationTests
    {
        private IConverter _converter;
        [TestInitialize]
        public void Initialize()
        {
            _converter = new Converter();
        }

        [TestMethod]
        public void ChequeController_Validate_Date()
        {
            ChequeModel ch = new ChequeModel()
            {
                Amount = 123,
                Payee = "Test"
            };

            var controller = new ChequeController(_converter);
            controller.ViewData.ModelState.Clear();
            controller.DisplayResult(ch);
            Assert.IsTrue(controller.ModelState.IsValid);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChequeController_Validate_Amount()
        {
            ChequeModel ch = new ChequeModel()
            {
                Amount = 0,
                Payee = "Test"
            };

            var controller = new ChequeController(_converter);
            controller.ViewData.ModelState.Clear();
            controller.DisplayResult(ch);
            Assert.IsTrue(controller.ModelState.IsValid);
        }
        [TestMethod]
        public void ChequeController_Validate_Payee()
        {
            ChequeModel ch = new ChequeModel()
            {
                Amount = 123,
                Payee = ""
            };

            var controller = new ChequeController(_converter);
            controller.ViewData.ModelState.Clear();
            controller.DisplayResult(ch);
            Assert.IsTrue(controller.ModelState.IsValid);
        }
    }
}
