﻿using Cheque.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cheque.Core.Implementation
{
    public class Converter : IConverter
    {
        private static string[] _ones = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

        private static string[] _teens = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };

        private static string[] _tens = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

        // US Numbering:
        private static string[] _thousands = { "", "thousand", "million", "billion", "trillion", "quadrillion" };
        bool convertFraction = false;
        public string Convert(double value)
        {
            string digits, text;
            bool showThousands = false;

            bool allZeros = true;
            if (value < 0)
                throw new ArgumentException("Value can not be negative");
            if (value == 0)
                throw new ArgumentException("Value can not be zero");

            // Use StringBuilder to build result
            StringBuilder builder = new StringBuilder();
            // Convert integer portion of value to string
            digits = ((long)value).ToString();

            // Traverse characters in reverse order
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                // Determine if ones, tens, or hundreds column
                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            // First digit in number (last in loop)
                            text = string.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            // This digit is part of "teen" value
                            text = string.Format("{0} ", _teens[ndigit]);
                            // Skip tens position
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            // Any non-zero digit
                            text = string.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            // This digit is zero. If digit in tens and hundreds
                            // column are also zero, don't show "thousands"
                            text = string.Empty;
                            // Test for non-zero digit in this grouping
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        // Show "thousands" if non-zero in grouping
                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                if (column > 0)
                                {
                                    text = string.Format("{0}{1}{2}", text, _thousands[column / 3], allZeros ? " " : ", ");
                                }
                                // Indicate non-zero digit encountered
                                allZeros = false;
                            }
                        }
                        builder.Insert(0, text);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            text = string.Format("{0} ", _tens[ndigit]);
                            builder.Insert(0, text);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            text = string.Format("{0} hundred ", _ones[ndigit]);
                            builder.Insert(0, text);
                        }
                        break;
                }
            }

            // Append fractional portion/cents
            double fraction = (value - (long)value) * 100;
            if (fraction > 0 && convertFraction == false)
            {
                convertFraction = true;
                builder.AppendFormat("dollars and {0:00} cents", Convert((long)fraction).ToLower());
            }
            else if (fraction == 0 && convertFraction == false)
            {
                builder.Append("dollars");
            }

            // Capitalize first letter
            return string.Format("{0}{1}", char.ToUpper(builder[0]), builder.ToString(1, builder.Length - 1)).TrimEnd();
        }

    }
}