﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cheque.Core.Contracts
{
    public interface IConverter
    {
        string Convert(double value);
    }
}
